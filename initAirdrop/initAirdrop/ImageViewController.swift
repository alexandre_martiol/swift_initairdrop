//
//  ImageViewController.swift
//  initAirdrop
//
//  Created by AlexM on 23/03/15.
//  Copyright (c) 2015 AMO. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    
    //It is necessary to create an array because it is sent.
    var imagesArray: [UIImage] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imagesArray.append(UIImage(named: "blastoise.jpg")!)
    }
    
    @IBAction func shareAction(sender: AnyObject) {
        var controller = UIActivityViewController(activityItems:imagesArray, applicationActivities: nil)
        
        self.presentViewController(controller, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  URLViewController.swift
//  initAirdrop
//
//  Created by AlexM on 23/03/15.
//  Copyright (c) 2015 AMO. All rights reserved.
//

import UIKit

class URLViewController: UIViewController, UIWebViewDelegate {
    @IBOutlet weak var urlText: UITextField!
    @IBOutlet weak var webView: UIWebView!
    
    //It is necessary to create an array because it is sent.
    var urlsArray: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        webView.delegate = self
    }
    
    @IBAction func loadAction(sender: AnyObject) {
        var request = NSURLRequest(URL: NSURL(string: urlText.text)!)
        self.webView.loadRequest(request)
        
        urlsArray.append(urlText.text)
    }
    
    @IBAction func shareAction(sender: AnyObject) {
        var controller = UIActivityViewController(activityItems:urlsArray, applicationActivities: nil)
        
        self.presentViewController(controller, animated: true, completion: nil)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
